package projectile;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.Files;
import com.badlogic.gdx.graphics.Color;

public class Main {
    public static void main(String[] args) {
        // Create window configuration
        LwjglApplicationConfiguration window = new LwjglApplicationConfiguration();

        // Configure window parameters
        window.addIcon("icon/256x256.png", Files.FileType.Local);
        window.addIcon("icon/128x128.png", Files.FileType.Local);
        window.addIcon("icon/64x64.png", Files.FileType.Local);
        window.addIcon("icon/32x32.png", Files.FileType.Local);
        window.addIcon("icon/16x16.png", Files.FileType.Local);
        window.title = "Projectile Motion Simulator";
        window.width = 1280;
        window.height = 720;
        window.resizable = false; // Disable resizing (issues when enabled)
        window.backgroundFPS = 60; // Game runs at 60 FPS when not in focus
        window.foregroundFPS = 60; // Game runs at 60 FPS when in focus
        window.initialBackgroundColor = Color.WHITE;

        // Create window
        new LwjglApplication(new ProjectileGame(), window);
    }
}
