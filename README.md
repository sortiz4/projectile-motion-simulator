# Projectile Motion Simulator
I made this little game for a software development class while I was an
undergraduate at UTSA back in 2015. It was my first project, so please keep
that in mind if you decide to take a look at the code. I've reformatted the
project and made a few small changes to simplify compilation.

## Requirements
- Linux, MacOS, or Windows
- Java 8

## Compilation
In addition to the requirements above, those seeking to compile must have...

- Gradle 4.2
- Java SDK 8

Once these requirements have been met, simply clone the repository and execute
`gradle desktop:dist`. An executable `jar` will be created under
`./desktop/build/libs/`.

## Controls

### User
```
[SPACEBAR] - Apply the velocity and torque (from sliders) to the ball.
[ESCAPE]   - Pauses or resumes the world state.
[E]        - Emits an object.
[D]        - Demits an object.
```

### Debug
```
[ARROW KEYS] - Moves ball in desired direction (uses slider values).
[BACKSPACE]  - Deletes all emitted objects of all types.
[EQUALS]     - Emits 100 objects (use with caution).
[MINUS]      - Demits 100 objects.
[1]          - Selects human emitter.
[2]          - Selects stick emitter.
[3]          - Selects ball emitter.
[4]          - Selects bird emitter.
[5]          - Selects box emitter.
```
