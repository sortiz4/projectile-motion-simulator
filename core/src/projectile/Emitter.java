package projectile;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import java.util.ArrayList;

/**
 * Emits a dynamic game object from a given location or randomly within a specified area.
 */
public class Emitter {
    public Stage stage;
    public World world;
    public String shapeType;
    public String texturePath;
    public Vector2 position;
    public Vector3 physical;

    public boolean random;
    public Vector2 domain;
    public Vector2 range;
    public int objectIndex;
    public ArrayList<DynamicObject> objectList;

    /**
     * Initializes the dynamic object details and specifies a spawn location.
     * @param stage
     * @param world
     * @param shapeType
     * @param texturePath
     * @param position
     * @param physical
     */
    public Emitter(
        Stage stage,
        World world,
        String shapeType,
        String texturePath,
        Vector2 position,
        Vector3 physical
    ) {
        objectIndex = -1;
        objectList = new ArrayList<>();
        random = false;
        this.stage = stage;
        this.world = world;
        this.position = position;
        this.physical = physical;
        this.shapeType = shapeType;
        this.texturePath = texturePath;
    }

    /**
     * Initializes the dynamic object details and randomizes the spawn area.
     * @param stage
     * @param world
     * @param shapeType
     * @param texturePath
     * @param domain
     * @param range
     * @param physical
     */
    public Emitter(
        Stage stage,
        World world,
        String shapeType,
        String texturePath,
        Vector2 domain,
        Vector2 range,
        Vector3 physical
    ) {
        objectIndex = -1;
        objectList = new ArrayList<>();
        random = true;
        this.stage = stage;
        this.world = world;
        this.range = range;
        this.domain = domain;
        this.physical = physical;
        this.shapeType = shapeType;
        this.texturePath = texturePath;
    }

    /**
     * Allows the position to be changed to an arbitrary location.
     * @param position
     */
    public void changePosition(Vector2 position) {
        this.position = position;
    }

    /**
     * Emits the object at the specified location or area.
     */
    public void emit() {
        if (random) {
            float x = domain.x + (float)(Math.random() * ((domain.y - domain.x) + 1));
            float y = range.x + (float)(Math.random() * ((range.y - range.x) + 1));
            position = new Vector2(x, y);
        }
        objectList.add(
            new DynamicObject(
                world,
                shapeType,
                texturePath,
                position,
                physical
            )
        );
        objectIndex = objectList.size() - 1;
        stage.addActor(objectList.get(objectIndex));
    }

    /**
     * Destroys the last item in the list.
     */
    public void demit() {
        if (objectIndex > -1) {
            world.destroyBody(objectList.get(objectIndex).body);
            objectList.get(objectIndex).remove();
            objectList.get(objectIndex).dispose();
            objectList.remove(objectIndex);
            objectIndex = objectList.size() - 1;
        }
    }

    /**
     * Disposes of all objects in the list.
     */
    public void dispose() {
        for (int i = objectIndex; i > -1; i--) {
            demit();
        }
    }
}
