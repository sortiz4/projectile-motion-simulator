package projectile;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * A non-interactive texture (for user interface elements and other decorations).
 */
public class SpriteObject extends Actor {
    public Sprite sprite;
    public Texture texture;

    /**
     * Creates a static, drawable, non-interactive texture (linear filtering).
     * @param texturePath
     * @param position
     */
    public SpriteObject(String texturePath, Vector2 position) {
        // Creates the sprite associated with the given texture path
        texture = new Texture(texturePath);
        texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        sprite = new Sprite(texture);

        // Set the position of center of the sprite to the given location
        sprite.setPosition(
            position.x - (sprite.getWidth() / 2.0f),
            position.y - (sprite.getHeight() / 2.0f)
        );
    }

    /**
     * Allows the stage to draw the sprite object's sprite and associated texture (called by Stage.draw).
     * @param batch
     * @param parentAlpha
     */
    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        // Only draw the sprite if defined
        if (sprite != null) {
            batch.draw(
                sprite,
                sprite.getX(),
                sprite.getY(),
                sprite.getOriginX(),
                sprite.getOriginY(),
                sprite.getWidth(),
                sprite.getHeight(),
                sprite.getScaleX(),
                sprite.getScaleY(),
                sprite.getRotation()
            );
        }
    }

    /**
     * Deallocates all disposable data associated with the sprite object.
     */
    public void dispose() {
        texture.dispose();
    }
}
