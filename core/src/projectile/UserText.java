package projectile;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

/**
 * A predefined text object with a modifiable color space (for the end user).
 */
public class UserText extends TextObject {
    /**
     * Creates an instance of this text object with basic information.
     * @param color
     * @param position
     * @param label
     */
    public UserText(Color color, Vector2 position, String label) {
        super("ui/font/consolas-20.fnt", "ui/font/consolas-20.png");
        super.setColor(color);
        super.setLabel(label);
        super.setPosition(position);
    }
}
