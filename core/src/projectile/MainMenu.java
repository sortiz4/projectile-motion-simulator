package projectile;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

/**
 * The first element the user sees when they start the application.
 */
public class MainMenu {
    public Skin skin;
    public TextButton exitButton;
    public TextButton startButton;
    public SpriteObject background;

    /**
     * Builds the main menu.
     */
    public MainMenu() {
        // Skin settings defined by JSON file
        skin = new Skin(Gdx.files.internal("ui/uiskin.json"));

        // Create menu background
        background = new SpriteObject(
            "ui/mainmenu.png",
            new Vector2(0.0f, 0.0f)
        );

        // Creates a start button
        startButton = new TextButton("Simulator", skin, "default");

        // Establishes button properties (top right corner)
        startButton.setWidth(175f);
        startButton.setHeight(35f);
        startButton.setPosition(
            -(startButton.getWidth() / 2.0f),
            -(startButton.getHeight() / 2.0f)
        );

        // Creates an exit button
        exitButton = new TextButton("Exit", skin, "default");

        // Establishes button properties (top right corner)
        exitButton.setWidth(175f);
        exitButton.setHeight(35f);
        exitButton.setPosition(
            -(exitButton.getWidth() / 2.0f),
            startButton.getY() - startButton.getHeight() - 10.0f
        );
    }

    /**
     * Adds all main menu actors to the given stage (order matters).
     * @param stage
     */
    public void addActors(Stage stage) {
        stage.addActor(background);
        stage.addActor(startButton);
        stage.addActor(exitButton);
    }

    /**
     * Removes all main menu actors from the most recent stage (order does not matter).
     */
    public void removeActors() {
        background.remove();
        startButton.remove();
        exitButton.remove();
    }

    /**
     * Deallocates all disposable data associated with the main menu.
     */
    public void dispose() {
        skin.dispose();
        background.dispose();
    }
}
