package projectile;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;

/**
 * A dynamic object assumes that a body and texture are both created.
 */
public class DynamicObject extends GameObject {
    /**
     * Creates a textured dynamic object (can only be a circle or polygon).
     * @param world
     * @param shapeType
     * @param texturePath
     * @param position
     * @param physical
     */
    public DynamicObject(World world, String shapeType, String texturePath, Vector2 position, Vector3 physical) {
        super(BodyDef.BodyType.DynamicBody);
        super.createSprite(texturePath);
        super.initializePosition(world, position);

        bodyDef.active = true;
        bodyDef.allowSleep = false;

        if (shapeType.equals("CircleShape")) {
            super.createCircleFixture(physical);
        } else if (shapeType.equals("PolygonShape")) {
            super.createPolygonFixture(physical);
        }
    }
}
