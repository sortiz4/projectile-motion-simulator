package projectile;

/**
 * Holds all user interface elements (main menu, pause menu).
 */
public class UserInterface {
    public MainMenu mainMenu;
    public PauseMenu pauseMenu;

    /**
     * Builds the main menu.
     */
    public UserInterface() {
        mainMenu = new MainMenu();
    }

    /**
     * Builds the pause menu.
     */
    public void addPauseMenu() {
        pauseMenu = new PauseMenu();
    }

    /**
     * Deallocates all disposable data associated with the user interface.
     */
    public void dispose() {
        if (mainMenu != null) {
            mainMenu.dispose();
        }
        if (pauseMenu != null) {
            pauseMenu.dispose();
        }
    }
}
