package projectile;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

/**
 * Handles physics, world state, bodies, user interface, and other positional data.
 */
public class ProjectileModel {
    public UserInterface userInterface;

    public Stage stage;
    public World world;

    public SpriteObject cannonCenter;
    public GameObject cannonRing;
    public GameObject cannon;
    public GameObject ball;
    public Observer ballObserver;

    public BoundingBox boundingBox;
    public TextObject debugText;

    public Emitter humanEmitter;
    public Emitter stickEmitter;
    public Emitter ballEmitter;
    public Emitter birdEmitter;
    public Emitter boxEmitter;

    public int currentEmitter = Constants.EMIT_HUMAN;
    public boolean mainInitialized = false;
    public boolean paused = true;

    /**
     * Builds the entirety of the stage and the world.
     */
    public ProjectileModel() {
        // Create physical world and define gravity (-98f => -9.8 m/s)
        world = new World(new Vector2(0.0f, -98.0f), true);

        // Create the Master UI (loads main menu by default)
        userInterface = new UserInterface();

        // Create the stage (size of window)
        stage = new Stage(
            new ExtendViewport(
                (float)Gdx.graphics.getWidth(),
                (float)Gdx.graphics.getHeight()
            )
        );

        // Add main menu elements to the stage
        userInterface.mainMenu.addActors(stage);
    }

    /**
     * Updates entire world state (physical bodies followed by sprites).
     */
    public void update() {
        if (!paused) {
            // Advance the world state (frame independent physics)
            world.step(Gdx.graphics.getDeltaTime(), 6, 2);

            // Update the stage (body and sprite positions)
            stage.act();
        } else {
            // World step rate set to zero
            world.step(0, 0, 0);
        }
    }

    /**
     * Deallocates all disposable data associated with the model.
     */
    public void dispose() {
        if (ball != null) {
            ball.dispose();
        }
        if (cannon != null) {
            cannon.dispose();
        }
        if (debugText != null) {
            debugText.dispose();
        }
        if (cannonRing != null) {
            cannonRing.dispose();
        }
        if (cannonCenter != null) {
            cannonCenter.dispose();
        }
        if (humanEmitter != null) {
            humanEmitter.dispose();
        }
        if (stickEmitter != null) {
            stickEmitter.dispose();
        }
        if (ballEmitter != null) {
            ballEmitter.dispose();
        }
        if (birdEmitter != null) {
            birdEmitter.dispose();
        }
        if (boxEmitter != null) {
            boxEmitter.dispose();
        }
        world.dispose();
        stage.dispose();
        userInterface.dispose();
    }

    /**
     * Loads the simulator portion of the application into the stage and world.
     */
    public void loadSimulatorStage() {
        // Create ball projectile (2.0m diameter)
        ball = new DynamicObject(
            world,
            "CircleShape",
            "player/ball-32.png",
            new Vector2(
                -((float)Gdx.graphics.getWidth() / 2.0f) + 92.0f,
                -((float)Gdx.graphics.getHeight() / 2.0f) + 46.0f
            ),
            // Physical properties: density, friction, restitution
            new Vector3(5.0f, 1.0f, 0.5f)
        );

        // Create cannon (angle of cannon = arctan(vy/vx))
        cannon = new StaticObject(
            world,
            "PolygonShape",
            "player/cannon-32.png",
            new Vector2(
                -((float)Gdx.graphics.getWidth() / 2.0f) + 46.0f,
                -((float)Gdx.graphics.getHeight() / 2.0f) + 46.0f
            )
        );

        // Create cannon ring
        cannonRing = new StaticObject(
            world,
            "CircleShape",
            "player/cannon-ring-32.png",
            new Vector2(
                -((float)Gdx.graphics.getWidth() / 2.0f) + 46.0f,
                -((float)Gdx.graphics.getHeight() / 2.0f) + 46.0f
            )
        );

        // Create cannon center point (floating sprite)
        cannonCenter = new SpriteObject(
            "player/cannon-center-32.png",
            new Vector2(
                -((float)Gdx.graphics.getWidth() / 2.0f) + 46.0f,
                -((float)Gdx.graphics.getHeight() / 2.0f) + 46.0f
            )
        );

        // Default constructor sets world edges to window edges
        boundingBox = new BoundingBox(world);

        // Create ball observer
        ballObserver = new BallObserver(world, ball.body);

        // Load the DebugText object
        debugText = new DebugText(
            Color.BLUE,
            new Vector2(
                -((float)Gdx.graphics.getWidth() / 2.0f) + 25.0f,
                ((float)Gdx.graphics.getHeight() / 2.0f) - 25.0f
            ),
            ballObserver
        );

        // Create the pause menu
        userInterface.addPauseMenu();

        // Create a human emitter for a random location
        humanEmitter = new Emitter(
            stage,
            world,
            "PolygonShape",
            "other/human-scale-16.png",
            new Vector2(
                // Domain of area
                -((float)Gdx.graphics.getWidth() / 2.0f) + 5.0f,
                ((float)Gdx.graphics.getWidth() / 2.0f) - 5.0f
            ),
            new Vector2(
                // Range of area
                -((float)Gdx.graphics.getHeight() / 2.0f) + 14.0f,
                ((float)Gdx.graphics.getHeight() / 2.0f) - 14.0f
            ),
            // Physical properties: density, friction, restitution
            new Vector3(2.0f, 1.0f, 0.25f)
        );

        // Create a stick emitter for a random location
        stickEmitter = new Emitter(
            stage,
            world,
            "PolygonShape",
            "other/stick-scale-16.png",
            new Vector2(
                // Domain of area
                -((float)Gdx.graphics.getWidth() / 2.0f) + 25.0f,
                ((float)Gdx.graphics.getWidth() / 2.0f) - 25.0f
            ),
            new Vector2(
                // Range of area
                -((float)Gdx.graphics.getHeight() / 2.0f) + 2.5f,
                ((float)Gdx.graphics.getHeight() / 2.0f) - 2.5f
            ),
            // Physical properties: density, friction, restitution
            new Vector3(0.5f, 1.0f, 0.10f)
        );

        // Create a ball emitter for a random location
        ballEmitter = new Emitter(
            stage,
            world,
            "CircleShape",
            "other/ball-scale-16.png",
            new Vector2(
                // Domain of area
                -((float)Gdx.graphics.getWidth() / 2.0f) + 8.0f,
                ((float)Gdx.graphics.getWidth() / 2.0f) - 8.0f
            ),
            new Vector2(
                // Range of area
                -((float)Gdx.graphics.getHeight() / 2.0f) + 8.0f,
                ((float)Gdx.graphics.getHeight() / 2.0f) - 8.0f
            ),
            // Physical properties: density, friction, restitution
            new Vector3(1.0f, 1.0f, 0.75f)
        );

        // Create a bird emitter for a random location
        birdEmitter = new Emitter(
            stage,
            world,
            "PolygonShape",
            "other/bird-scale-16.png",
            new Vector2(
                // Domain of area
                -((float)Gdx.graphics.getWidth() / 2.0f) + 13.0f,
                ((float)Gdx.graphics.getWidth() / 2.0f) - 13.0f
            ),
            new Vector2(
                // Range of area
                -((float)Gdx.graphics.getHeight() / 2.0f) + 8.0f,
                ((float)Gdx.graphics.getHeight() / 2.0f) - 8.0f
            ),
            // Physical properties: density, friction, restitution
            new Vector3(0.25f, 1.0f, 0.25f)
        );

        // Create a box emitter for a random location
        boxEmitter = new Emitter(
            stage,
            world,
            "PolygonShape",
            "other/box-scale-16.png",
            new Vector2(
                // Domain of area
                -((float)Gdx.graphics.getWidth() / 2.0f) + 12.0f,
                ((float)Gdx.graphics.getWidth() / 2.0f) - 12.0f
            ),
            new Vector2(
                // Range of area
                -((float)Gdx.graphics.getHeight() / 2.0f) + 12.0f,
                ((float)Gdx.graphics.getHeight() / 2.0f) - 12.0f
            ),
            // Physical properties: density, friction, restitution
            new Vector3(10.0f, 1.0f, 0.10f)
        );

        // Add player elements
        stage.addActor(ball);
        stage.addActor(cannonRing);
        stage.addActor(cannon);
        stage.addActor(cannonCenter);

        // Add debug statistics
        stage.addActor(debugText);

        // Add bounding box
        stage.addActor(boundingBox);

        // Add the pause menu elements
        userInterface.pauseMenu.addActors(stage);
    }
}
