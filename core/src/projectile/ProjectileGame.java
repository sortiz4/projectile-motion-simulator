package projectile;

import com.badlogic.gdx.ApplicationListener;

public class ProjectileGame implements ApplicationListener {
    private ProjectileModel model;
    private ProjectileView view;
    private ProjectileController controller;

    /**
     * Create is called when the application starts (initializes all data).
     */
    public void create() {
        model = new ProjectileModel();
        view = new ProjectileView(model);
        controller = new ProjectileController(model);

        // Initialize listeners
        controller.registerListeners("MainMenu");
    }

    /**
     * Render is called every frame (limited by frame rate).
     */
    public void render() {
        view.update(); // Render all physical data first
        model.update(); // Update all physical data second
    }

    /**
     * Resize is called when the window is resized (currently disabled).
     * @param width
     * @param height
     */
    public void resize(int width, int height) {
    }

    /**
     * Pause is called when the window loses focus.
     */
    public void pause() {
    }

    /**
     * Resume is called when the window is brought into focus.
     */
    public void resume() {
    }

    /**
     * Dispose is called when the window is closed.
     */
    public void dispose() {
        model.dispose(); // Dispose of all physical data
        view.dispose(); // Dispose of all rendering data
    }
}
