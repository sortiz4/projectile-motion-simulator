package projectile;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Define the boundaries of the world.
 */
public class BoundingBox extends Actor {
    private GameObject top;
    private GameObject left;
    private GameObject right;
    private GameObject bottom;

    /**
     * Aligns the world edges to the window edges.
     * @param world
     */
    public BoundingBox(World world) {
        // Get window dimensions
        float width = (float)Gdx.graphics.getWidth() / Constants.PIXELS_TO_METERS;
        float height = (float)Gdx.graphics.getHeight() / Constants.PIXELS_TO_METERS;

        // Set world edges
        top = new StaticObject(
            world,
            new Vector2(-width / 2, height / 2),
            new Vector2(width / 2, height / 2)
        );
        left = new StaticObject(
            world,
            new Vector2(-width / 2, -height / 2),
            new Vector2(-width / 2, height / 2)
        );
        right = new StaticObject(
            world,
            new Vector2(width / 2, -height / 2),
            new Vector2(width / 2, height / 2)
        );
        bottom = new StaticObject(
            world,
            new Vector2(-width / 2, -height / 2),
            new Vector2(width / 2, -height / 2)
        );
    }
}
