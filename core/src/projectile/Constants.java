package projectile;

import com.badlogic.gdx.graphics.Color;

/**
 * All constants used throughout the project.
 */
public final class Constants {
    // Pixels per meter constant (16 pixels => 1 meter)
    public static final float PIXELS_TO_METERS = 16;

    // Spacing constants (used by user interface elements)
    public static final float DESCRIPTOR_SPACING = 230.0f; // Spacing for menu descriptors
    public static final float WINDOW_SPACING = 25.0f; // Spacing from window edges
    public static final float ITEM_SPACING = 10.0f; // Spacing between items

    // Color constants (used by user interface elements)
    public static final Color BRIGHT_ORANGE = new Color(0xFF8A00FF);
    public static final Color BRIGHT_GREEN = new Color(0x00D651FF);
    public static final Color BRIGHT_BLUE = new Color(0x008AFFFF);

    // Emitter constants (used by projectile controller)
    public static final int EMIT_HUMAN = 1; // Normal human
    public static final int EMIT_STICK = 2; // Normal stick
    public static final int EMIT_BALL = 3; // Bouncy ball
    public static final int EMIT_BIRD = 4; // Light bird
    public static final int EMIT_BOX = 5; // Heavy box

    // Boolean constants (for turning things on and off)
    public static final boolean DEBUG_CONTROL = true;
    public static final boolean DEBUG_RENDER = false;
}
