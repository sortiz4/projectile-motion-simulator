package projectile;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;

/**
 * Handles the drawing and rendering of all application data.
 */
public class ProjectileView {
    private ProjectileModel model;
    private SpriteBatch batch;
    public Box2DDebugRenderer debugRenderer;
    public Matrix4 debugMatrix;

    /**
     * Creates the renderer and targets the model.
     * @param model
     */
    public ProjectileView(ProjectileModel model) {
        this.model = model;

        // Create sprite batch
        batch = new SpriteBatch();

        // Create debug renderer
        if (Constants.DEBUG_RENDER) {
            debugRenderer = new Box2DDebugRenderer();
            debugRenderer.setDrawBodies(true);
        }

        // Translate camera to center of screen (temporary fix?)
        ((OrthographicCamera) model.stage.getCamera()).translate(
            -((float)Gdx.graphics.getWidth() / 2.0f),
            -((float)Gdx.graphics.getHeight() / 2.0f)
        );
    }

    /**
     * Updates the entire application window by drawing every actor on the stage.
     */
    public void update() {
        // Refresh screen state (empty previous frame buffer)
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Set the projection matrix of of this sprite batch to match the stage camera
        batch.setProjectionMatrix(model.stage.getCamera().combined);

        // Get projection matrix from this sprite batch and scale to meters
        debugMatrix = batch.getProjectionMatrix().cpy().scale(
            Constants.PIXELS_TO_METERS,
            Constants.PIXELS_TO_METERS,
            0.0f
        );

        // Draw the sprites
        batch.begin();
        model.stage.draw();
        batch.end();

        // Update debug renderer
        if (Constants.DEBUG_RENDER) {
            debugRenderer.render(model.world, debugMatrix);
        }
    }

    /**
     * Deallocates all disposable data associated with the render.
     */
    public void dispose() {
        if (Constants.DEBUG_RENDER) {
            debugRenderer.dispose();
        }
        batch.dispose();
    }
}
