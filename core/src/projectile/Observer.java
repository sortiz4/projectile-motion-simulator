package projectile;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Observes a world (bodies should be defined in a subclass).
 */
public class Observer {
    public World world;

    /**
     * Assigns a world to observe.
     * @param world
     */
    public Observer(World world) {
        this.world = world;
    }

    /**
     * Prints information about the world and framerate.
     * @return String
     */
    public String toString() {
        return String.join(
            "",

            // First line: gravity, number of bodies
            String.format(
                " WORLD: [Gravity(%3.1f, %3.1f), Bodies(%d)]\n",
                world.getGravity().x,
                world.getGravity().y,
                world.getBodyCount()
            ),

            // Second line: framerate
            String.format(
                "RENDER: [FPS(%d)]",
                Gdx.graphics.getFramesPerSecond()
            )
        );
    }
}
