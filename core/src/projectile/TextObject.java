package projectile;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Simplifies the creation and destruction of text objects.
 */
public class TextObject extends Actor {
    public Observer observer;
    public String observerString;
    public String label;
    public Vector2 position;
    public BitmapFont bitmapFont;

    /**
     * Creates a text object given a bitmap font path (called by subclasses).
     * @param fntPath
     * @param pngPath
     */
    public TextObject(String fntPath, String pngPath) {
        bitmapFont = new BitmapFont(
            Gdx.files.internal(fntPath),
            Gdx.files.internal(pngPath),
            false
        );
        observerString = "";
        label = "";
    }

    /**
     * Allows the stage to stage the text object's bitmap font (called by Stage.draw).
     * @param batch
     * @param parentAlpha
     */
    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        // Save the string result of the observer (if it exists)
        if (observer != null) {
            observerString = observer.toString();
        }

        // Draw the bitmap font
        bitmapFont.draw(batch, label + observerString, position.x, position.y);
    }

    /**
     * Deallocates all disposable data associated with the text object.
     */
    public void dispose() {
        bitmapFont.dispose();
    }

    /**
     * Sets the bitmap font color (called by subclasses).
     * @param color
     */
    public void setColor(Color color) {
        bitmapFont.setColor(color);
    }

    /**
     * Sets the label for this text object (should not be changed if set).
     * @param label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Sets the position of the text object's bottom-left point in the window.
     * @param position
     */
    public void setPosition(Vector2 position) {
        this.position = position;
    }

    /**
     * Assigns an observer for this text object to use.
     * @param observer
     */
    public void setObserver(Observer observer) {
        this.observer = observer;
    }

    /**
     * Saves the string result of the observer (if it exists).
     * @param observerString
     */
    public void setObserverString(String observerString) {
        this.observerString = observerString;
    }
}
