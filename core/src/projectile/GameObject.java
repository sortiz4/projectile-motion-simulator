package projectile;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Simplifies the creation and destruction of game objects.
 */
public class GameObject extends Actor {
    public Body body;
    public Sprite sprite;
    public Texture texture;
    public BodyDef bodyDef;
    public FixtureDef fixtureDef;

    /**
     * Defines dynamic or static body type (called by subclasses).
     * @param bodyType
     */
    public GameObject(BodyType bodyType) {
        bodyDef = new BodyDef();
        bodyDef.type = bodyType;
    }

    /**
     * Creates a sprite associated with the given texture path (defaults to linear texture filtering).
     * @param texturePath
     */
    public void createSprite(String texturePath) {
        texture = new Texture(texturePath);
        texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        sprite = new Sprite(texture);
    }

    /**
     * Initializes the position of the center of the sprite in pixels, then updates the position of the center of the
     * body to the corresponding position in meters.
     * @param world
     * @param position
     */
    public void initializePosition(World world, Vector2 position) {
        if (sprite != null) {
            // Set the position of center of the sprite to the given location
            sprite.setPosition(
                position.x - (sprite.getWidth() / 2),
                position.y - (sprite.getHeight() / 2)
            );

            // Set the position of the center of the body to the corresponding location
            bodyDef.position.set(
                // Add half-width of the sprite and convert to meters
                (sprite.getX() + (sprite.getWidth() / 2.0f)) / Constants.PIXELS_TO_METERS,
                // Add half-height of the sprite and convert to meters
                (sprite.getY() + (sprite.getHeight() / 2.0f)) / Constants.PIXELS_TO_METERS
            );
        } else {
            // Set the position of the center of the body to the given location
            bodyDef.position.set(
                // Convert to meters
                position.x / Constants.PIXELS_TO_METERS,
                // Convert to meters
                position.y / Constants.PIXELS_TO_METERS
            );
        }

        // Place the body in the world using bodyDef
        body = world.createBody(bodyDef);
    }

    /**
     * Creates a circular fixture given a physical vector (may be null when creating static object).
     * @param physical
     */
    public void createCircleFixture(Vector3 physical) {
        // Create a circle shape
        CircleShape circleShape = new CircleShape();

        // Sets shape size equivalent to sprite size
        circleShape.setRadius((sprite.getWidth() / 2.0f) / Constants.PIXELS_TO_METERS);

        // Create a fixture definition for a circle
        fixtureDef = new FixtureDef();
        fixtureDef.shape = circleShape;

        // Define physical properties if the physical vector is not null (is dynamic)
        if (physical != null) {
            // Density (kg*m^2)
            fixtureDef.density = physical.x;

            // Coefficient of friction
            fixtureDef.friction = physical.y;

            // Coefficient of restitution (bounciness)
            fixtureDef.restitution = physical.z;
        }
        body.createFixture(fixtureDef);

        // Shape is disposable so deallocate
        circleShape.dispose();
    }

    /**
     * Creates a polygon (box) fixture given a physical vector (may be null when creating static object).
     * @param physical
     */
    public void createPolygonFixture(Vector3 physical) {
        // Define the physical shape.
        PolygonShape polygonShape = new PolygonShape();

        // Set size equivalent to sprite
        polygonShape.setAsBox(
            (sprite.getWidth() / 2.0f) / Constants.PIXELS_TO_METERS,
            (sprite.getHeight() / 2.0f) / Constants.PIXELS_TO_METERS
        );

        // Create a fixture definition for a polygon
        fixtureDef = new FixtureDef();
        fixtureDef.shape = polygonShape;

        // Define physical properties if the physical vector is not null (is dynamic)
        if (physical != null) {
            // Density (kg*m^2)
            fixtureDef.density = physical.x;

            // Coefficient of friction
            fixtureDef.friction = physical.y;

            // Coefficient of restitution (bounciness)
            fixtureDef.restitution = physical.z;
        }
        body.createFixture(fixtureDef);

        // Shape is disposable so deallocate
        polygonShape.dispose();
    }

    /**
     * Creates an edge fixture. Always assumed to be static without a texture or sprite (use a polygon otherwise).
     * @param left - Should be left-most point (x, y)
     * @param right - Should be right-most point (x, y)
     */
    public void createEdgeFixture(Vector2 left, Vector2 right) {
        // Create edge shape and set position
        EdgeShape edgeShape = new EdgeShape();
        edgeShape.set(left.x, left.y, right.x, right.y);

        // Create a fixture definition for an edge
        fixtureDef = new FixtureDef();
        fixtureDef.shape = edgeShape;
        body.createFixture(fixtureDef);

        // Shape is disposable so deallocate
        edgeShape.dispose();
    }

    /**
     * Updates the position of the center of the sprite to the center of the corresponding body (called by Stage.act).
     */
    @Override
    public void act(float deltaTime) {
        super.act(deltaTime);

        // Only update the sprite if defined
        if (sprite != null) {
            // Set position of center of sprite to location of body
            sprite.setPosition(
                // Convert to pixels and subtract half-width
                (body.getPosition().x * Constants.PIXELS_TO_METERS) - (sprite.getWidth() / 2.0f),
                // Convert to pixels and subtract half-height
                (body.getPosition().y * Constants.PIXELS_TO_METERS) - (sprite.getHeight() / 2.0f)
            );

            // Update the rotation of the sprite to rotation of body
            sprite.setRotation((float)Math.toDegrees(body.getAngle()));
        }
    }

    /**
     * Allows the stage to draw the game object's sprite and associated texture (called by Stage.draw).
     * @param batch
     * @param parentAlpha
     */
    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        // Only draw the sprite if it is defined
        if (sprite != null) {
            batch.draw(
                sprite,
                sprite.getX(),
                sprite.getY(),
                sprite.getOriginX(),
                sprite.getOriginY(),
                sprite.getWidth(),
                sprite.getHeight(),
                sprite.getScaleX(),
                sprite.getScaleY(),
                sprite.getRotation()
            );
        }
    }

    /**
     * Deallocates all disposable data associated with the game object.
     */
    public void dispose() {
        if (texture != null) {
            texture.dispose();
        }
    }
}
