package projectile;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;

/**
 * A ball observer monitors a circular body in a given world.
 */
public class BallObserver extends Observer {
    public Body body;

    /**
     * Assigns a world to observe and a body to observe.
     * @param world
     * @param body
     */
    public BallObserver(World world, Body body) {
        super(world);
        this.body = body;
    }

    /**
     * Prints information about this body.
     * @return String
     */
    public String toString() {
        return String.join(
            "",

            // First line: angular velocity, velocity, position
            "  BALL: [",
            String.format(
                "AngularVelocity(%3.1f), Velocity(%3.1f, %3.1f), Position(%3.1f, %3.1f)",
                body.getAngularVelocity(),
                body.getLinearVelocity().x,
                body.getLinearVelocity().y,
                body.getPosition().x,
                body.getPosition().y
            ),
            "]\n        ",

            // Second line: density, friction, restitution
            "[",
            String.format(
                "Mass(%4.2f), Density(%3.1f), Friction(%3.1f), Restitution(%3.1f)",
                body.getMass(),
                body.getFixtureList().first().getDensity(),
                body.getFixtureList().first().getFriction(),
                body.getFixtureList().first().getRestitution()
            ),
            "]\n",

            // Fetch and display world status
            super.toString()
        );
    }
}
