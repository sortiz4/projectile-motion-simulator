package projectile;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Handles the modification of model data through a user interface.
 */
public class ProjectileController implements InputProcessor {
    private ProjectileModel model;
    private InputMultiplexer multiplexer;

    /**
     * Creates the controller and targets the model.
     * @param model
     */
    public ProjectileController(ProjectileModel model) {
        this.model = model;

        // Setup input processors
        multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(this);
        multiplexer.addProcessor(model.stage);
        Gdx.input.setInputProcessor(multiplexer);
    }

    /**
     * Registers menu event listeners.
     * @param code
     */
    public void registerListeners(String code) {
        if (code.equals("MainMenu")) {
            // Create start button listener
            model.userInterface.mainMenu.startButton.addListener(
                new ClickListener() {
                    public void clicked(InputEvent event, float x, float y) {
                        // Remove the main menu from the stage
                        model.userInterface.mainMenu.removeActors();

                        // Load the simulator portion into the stage
                        model.loadSimulatorStage();

                        // Register pause menu listeners
                        registerListeners("PauseMenu");

                        // Set the model initialized state to true
                        model.mainInitialized = true;
                    }
                }
            );

            // Create exit button listener
            model.userInterface.mainMenu.exitButton.addListener(
                new ClickListener() {
                    public void clicked(InputEvent event, float x, float y) {
                        // Exit the application
                        Gdx.app.exit();
                    }
                }
            );
        } else if (code.equals("PauseMenu")) {
            // Create restitution slider listener
            model.userInterface.pauseMenu.restitutionSlider.addListener(
                new ChangeListener() {
                    public void changed(ChangeEvent event, Actor actor) {
                        // Display ball restitution
                        model.userInterface.pauseMenu.restitutionLabel.setObserverString(
                            String.format("%4.2f", model.userInterface.pauseMenu.restitutionSlider.getValue())
                        );

                        // Change ball restitution
                        model.ball.body.getFixtureList().get(0).setRestitution(
                            model.userInterface.pauseMenu.restitutionSlider.getValue()
                        );
                        model.ball.body.resetMassData();
                    }
                }
            );

            // Create friction slider listener
            model.userInterface.pauseMenu.frictionSlider.addListener(
                new ChangeListener() {
                    public void changed(ChangeEvent event, Actor actor) {
                        // Display ball friction
                        model.userInterface.pauseMenu.frictionLabel.setObserverString(
                            String.format("%4.2f", model.userInterface.pauseMenu.frictionSlider.getValue())
                        );

                        // Change ball friction
                        model.ball.body.getFixtureList().get(0).setFriction(
                            model.userInterface.pauseMenu.frictionSlider.getValue()
                        );
                        model.ball.body.resetMassData();
                    }
                }
            );

            // Create density slider listener
            model.userInterface.pauseMenu.densitySlider.addListener(
                new ChangeListener() {
                    public void changed(ChangeEvent event, Actor actor) {
                        // Display ball density
                        model.userInterface.pauseMenu.densityLabel.setObserverString(
                            String.format("%4.2f", model.userInterface.pauseMenu.densitySlider.getValue())
                        );

                        // Change ball density
                        model.ball.body.getFixtureList().get(0).setDensity(
                            model.userInterface.pauseMenu.densitySlider.getValue()
                        );
                        model.ball.body.resetMassData();
                    }
                }
            );

            // Create velocity `x` slider listener
            model.userInterface.pauseMenu.vxSlider.addListener(
                new ChangeListener() {
                    public void changed(ChangeEvent event, Actor actor) {
                        // Display ball velocity `x`
                        model.userInterface.pauseMenu.vxLabel.setObserverString(
                            String.format("%4.2f", model.userInterface.pauseMenu.vxSlider.getValue())
                        );

                        // Change angle of cannon
                        float angle = (float)Math.atan2(
                            model.userInterface.pauseMenu.vySlider.getValue(),
                            model.userInterface.pauseMenu.vxSlider.getValue()
                        );
                        model.cannon.body.setTransform(model.cannon.body.getPosition(), angle);
                    }
                }
            );

            // Create velocity `y` slider listener
            model.userInterface.pauseMenu.vySlider.addListener(
                new ChangeListener() {
                    public void changed(ChangeEvent event, Actor actor) {
                        // Display ball velocity `y`
                        model.userInterface.pauseMenu.vyLabel.setObserverString(
                            String.format("%4.2f", model.userInterface.pauseMenu.vySlider.getValue())
                        );

                        // Change angle of cannon
                        float angle = (float)Math.atan2(
                            model.userInterface.pauseMenu.vySlider.getValue(),
                            model.userInterface.pauseMenu.vxSlider.getValue()
                        );
                        model.cannon.body.setTransform(model.cannon.body.getPosition(), angle);
                    }
                }
            );

            // Create torque slider listener
            model.userInterface.pauseMenu.torqueSlider.addListener(
                new ChangeListener() {
                    public void changed(ChangeEvent event, Actor actor) {
                        // Display ball torque
                        model.userInterface.pauseMenu.torqueLabel.setObserverString(
                            String.format("%4.2f", model.userInterface.pauseMenu.torqueSlider.getValue())
                        );
                    }
                }
            );

            // Create gravity `x` slider listener
            model.userInterface.pauseMenu.gxSlider.addListener(
                new ChangeListener() {
                    public void changed(ChangeEvent event, Actor actor) {
                        // Display gravity `x`
                        model.userInterface.pauseMenu.gxLabel.setObserverString(
                            String.format("%4.2f", model.userInterface.pauseMenu.gxSlider.getValue())
                        );

                        // Change gravity `x`
                        Vector2 gravity = model.world.getGravity();
                        model.world.setGravity(
                            new Vector2(
                                model.userInterface.pauseMenu.gxSlider.getValue() * 10.0f,
                                gravity.y
                            )
                        );
                    }
                }
            );

            // Create gravity `y` slider listener
            model.userInterface.pauseMenu.gySlider.addListener(
                new ChangeListener() {
                    public void changed(ChangeEvent event, Actor actor) {
                        // Display gravity `y`
                        model.userInterface.pauseMenu.gyLabel.setObserverString(
                            String.format("%4.2f", model.userInterface.pauseMenu.gySlider.getValue())
                        );

                        // Change gravity `y`
                        Vector2 gravity = model.world.getGravity();
                        model.world.setGravity(
                            new Vector2(
                                gravity.x,
                                model.userInterface.pauseMenu.gySlider.getValue() * 10.0f
                            )
                        );
                    }
                }
            );

            // Create resume button listener
            model.userInterface.pauseMenu.resumeButton.addListener(
                new ClickListener() {
                    public void clicked(InputEvent event, float x, float y) {
                        model.userInterface.pauseMenu.removeActors();
                        model.paused = false;
                    }
                }
            );

            // Create exit button listener
            model.userInterface.pauseMenu.exitButton.addListener(
                new ClickListener() {
                    public void clicked(InputEvent event, float x, float y) {
                        Gdx.app.exit();
                    }
                }
            );
        }
    }

    /**
     * Handles all keyboard presses.
     * @param keyCode
     * @return boolean
     */
    @Override
    public boolean keyDown(int keyCode) {
        // These key presses are only allowed if the model state has been initialized
        if (model.mainInitialized) {
            // Pause or resume the game on escape key press
            if (keyCode == Keys.ESCAPE) {
                if (model.paused) {
                    model.userInterface.pauseMenu.removeActors();
                    model.paused = false;
                } else {
                    model.userInterface.pauseMenu.addActors(model.stage);
                    model.paused = true;
                }
            }

            // Selects object to emit
            if (keyCode == Keys.NUM_1) {
                model.currentEmitter = Constants.EMIT_HUMAN;
            }
            if (keyCode == Keys.NUM_2) {
                model.currentEmitter = Constants.EMIT_STICK;
            }
            if (keyCode == Keys.NUM_3) {
                model.currentEmitter = Constants.EMIT_BALL;
            }
            if (keyCode == Keys.NUM_4) {
                model.currentEmitter = Constants.EMIT_BIRD;
            }
            if (keyCode == Keys.NUM_5) {
                model.currentEmitter = Constants.EMIT_BOX;
            }

            // These key presses are only allowed if the model has not been paused
            if (!model.paused) {
                // Reapply the forces on space bar press
                if (keyCode == Keys.SPACE) {
                    // Save the slider values to local variables (must scale appropriately)
                    Vector2 velocity = new Vector2(
                        model.userInterface.pauseMenu.vxSlider.getValue() * 10.0f,
                        model.userInterface.pauseMenu.vySlider.getValue() * 10.0f
                    );
                    float torque = model.userInterface.pauseMenu.torqueSlider.getValue() * 10000.0f;

                    // Calculate cannon mouth position given it's angle
                    float h = (model.cannon.sprite.getWidth() / 2.0f);
                    float y = (float)Math.sin(model.cannon.body.getAngle()) * h;
                    float x = (float)Math.cos(model.cannon.body.getAngle()) * h;

                    // Calculate world position of cannon mouth
                    Vector2 localPoint = new Vector2(0.0f, 0.0f);
                    localPoint.y = model.cannon.body.getWorldCenter().y + (y / Constants.PIXELS_TO_METERS);
                    localPoint.x = model.cannon.body.getWorldCenter().x + (x / Constants.PIXELS_TO_METERS);

                    // Reset ball position to cannon mouth
                    model.ball.body.setTransform(localPoint, 0.0f);

                    // Apply the forces to the body
                    model.ball.body.applyTorque(torque, true); // Apply torque (Torque, T=rF)
                    model.ball.body.setLinearVelocity(velocity); // Apply velocity (Velocity, V=d/t)
                }

                // Emit an object
                if (keyCode == Keys.E) {
                    if (model.currentEmitter == Constants.EMIT_HUMAN) {
                        model.humanEmitter.emit();
                    } else if (model.currentEmitter == Constants.EMIT_STICK) {
                        model.stickEmitter.emit();
                    } else if (model.currentEmitter == Constants.EMIT_BALL) {
                        model.ballEmitter.emit();
                    } else if (model.currentEmitter == Constants.EMIT_BIRD) {
                        model.birdEmitter.emit();
                    } else if (model.currentEmitter == Constants.EMIT_BOX) {
                        model.boxEmitter.emit();
                    }
                }

                // Demit an object
                if (keyCode == Keys.D) {
                    if (model.currentEmitter == Constants.EMIT_HUMAN) {
                        model.humanEmitter.demit();
                    } else if (model.currentEmitter == Constants.EMIT_STICK) {
                        model.stickEmitter.demit();
                    } else if (model.currentEmitter == Constants.EMIT_BALL) {
                        model.ballEmitter.demit();
                    } else if (model.currentEmitter == Constants.EMIT_BIRD) {
                        model.birdEmitter.demit();
                    } else if (model.currentEmitter == Constants.EMIT_BOX) {
                        model.boxEmitter.demit();
                    }
                }

                // These key presses are only allowed if debugging is turned on
                if (Constants.DEBUG_CONTROL) {
                    // Emits 100 objects
                    if (keyCode == Keys.EQUALS) {
                        for (int i = 0; i < 100; i++) {
                            if (model.currentEmitter == Constants.EMIT_HUMAN) {
                                model.humanEmitter.emit();
                            } else if (model.currentEmitter == Constants.EMIT_STICK) {
                                model.stickEmitter.emit();
                            } else if (model.currentEmitter == Constants.EMIT_BALL) {
                                model.ballEmitter.emit();
                            } else if (model.currentEmitter == Constants.EMIT_BIRD) {
                                model.birdEmitter.emit();
                            } else if (model.currentEmitter == Constants.EMIT_BOX) {
                                model.boxEmitter.emit();
                            }
                        }
                    }

                    // Demits 100 objects
                    if (keyCode == Keys.MINUS) {
                        for (int i = 0; i < 100; i++) {
                            if (model.currentEmitter == Constants.EMIT_HUMAN) {
                                model.humanEmitter.demit();
                            } else if (model.currentEmitter == Constants.EMIT_STICK) {
                                model.stickEmitter.demit();
                            } else if (model.currentEmitter == Constants.EMIT_BALL) {
                                model.ballEmitter.demit();
                            } else if (model.currentEmitter == Constants.EMIT_BIRD) {
                                model.birdEmitter.demit();
                            } else if (model.currentEmitter == Constants.EMIT_BOX) {
                                model.boxEmitter.demit();
                            }
                        }
                    }

                    // Demits all emitted objects
                    if (keyCode == Keys.BACKSPACE) {
                        int humanCount = model.humanEmitter.objectIndex;
                        int stickCount = model.stickEmitter.objectIndex;
                        int ballCount = model.ballEmitter.objectIndex;
                        int birdCount = model.birdEmitter.objectIndex;
                        int boxCount = model.boxEmitter.objectIndex;
                        for (int i = -1; i < humanCount; i++) {
                            model.humanEmitter.demit();
                        }
                        for (int i = -1; i < stickCount; i++) {
                            model.stickEmitter.demit();
                        }
                        for (int i = -1; i < ballCount; i++) {
                            model.ballEmitter.demit();
                        }
                        for (int i = -1; i < birdCount; i++) {
                            model.birdEmitter.demit();
                        }
                        for (int i = -1; i < boxCount; i++) {
                            model.boxEmitter.demit();
                        }
                    }

                    // Allows movement of ball in any direction (uses slider values)
                    if (keyCode == Keys.UP) {
                        // Apply velocity (Velocity, V=d/t)
                        model.ball.body.setLinearVelocity(
                            new Vector2(
                                model.ball.body.getLinearVelocity().x,
                                (
                                    model.ball.body.getLinearVelocity().y +
                                    (model.userInterface.pauseMenu.vxSlider.getValue() * 10.0f)
                                )
                            )
                        );
                    }
                    if (keyCode == Keys.DOWN) {
                        // Apply velocity (Velocity, V=d/t)
                        model.ball.body.setLinearVelocity(
                            new Vector2(
                                model.ball.body.getLinearVelocity().x,
                                (
                                    model.ball.body.getLinearVelocity().y -
                                    (model.userInterface.pauseMenu.vxSlider.getValue() * 10.0f)
                                )
                            )
                        );
                    }
                    if (keyCode == Keys.LEFT) {
                        // Apply velocity (Velocity, V=d/t)
                        model.ball.body.setLinearVelocity(
                            new Vector2(
                                (
                                    model.ball.body.getLinearVelocity().x -
                                    (model.userInterface.pauseMenu.vxSlider.getValue() * 10.0f)
                                ),
                                model.ball.body.getLinearVelocity().y
                            )
                        );
                    }
                    if (keyCode == Keys.RIGHT) {
                        // Apply velocity (Velocity, V=d/t)
                        model.ball.body.setLinearVelocity(
                            new Vector2(
                                (
                                    model.ball.body.getLinearVelocity().x +
                                    (model.userInterface.pauseMenu.vxSlider.getValue() * 10.0f)
                                ),
                                model.ball.body.getLinearVelocity().y
                            )
                        );
                    }
                }
            }
        }
        return true;
    }

    @Override
    public boolean keyUp(int i) {
        return false;
    }

    @Override
    public boolean keyTyped(char c) {
        return false;
    }

    @Override
    public boolean touchDown(int i, int i1, int i2, int i3) {
        return false;
    }

    @Override
    public boolean touchUp(int i, int i1, int i2, int i3) {
        return false;
    }

    @Override
    public boolean touchDragged(int i, int i1, int i2) {
        return false;
    }

    @Override
    public boolean mouseMoved(int i, int i1) {
        return false;
    }

    @Override
    public boolean scrolled(int i) {
        return false;
    }
}
