package projectile;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

/**
 * Shown to the user every time the escape key is pressed.
 */
public class PauseMenu {
    public Skin skin;
    public TextButton resumeButton;
    public TextButton exitButton;
    public SpriteObject background;

    public TextObject ballDescriptor;
    public TextObject cannonDescriptor;
    public TextObject worldDescriptor;

    public Slider densitySlider;
    public TextObject densityLabel;

    public Slider restitutionSlider;
    public TextObject restitutionLabel;

    public Slider frictionSlider;
    public TextObject frictionLabel;

    public Slider vxSlider;
    public Slider vySlider;
    public TextObject vxLabel;
    public TextObject vyLabel;

    public Slider torqueSlider;
    public TextObject torqueLabel;

    public Slider gxSlider;
    public TextObject gxLabel;
    public Slider gySlider;
    public TextObject gyLabel;

    /**
     * Builds the pause menu.
     */
    public PauseMenu() {
        float halfWidth = (float)Gdx.graphics.getWidth() / 2.0f;
        float halfHeight = (float)Gdx.graphics.getHeight() / 2.0f;

        // Skin settings defined by JSON file
        skin = new Skin(Gdx.files.internal("ui/uiskin.json"));

        // Create menu background
        background = new SpriteObject("ui/pausemenu.png", new Vector2(0.0f, 0.0f));

        // Create the cannonball descriptor text object
        ballDescriptor = new UserText(
            Constants.BRIGHT_ORANGE,
            new Vector2(0.0f, 0.0f),
            "Cannonball Properties"
        );
        ballDescriptor.setPosition(
            new Vector2(
                halfWidth - Constants.DESCRIPTOR_SPACING - Constants.WINDOW_SPACING,
                halfHeight - Constants.WINDOW_SPACING
            )
        );

        // Create the restitution slider
        restitutionSlider = new Slider(0.0f, 2.5f, 0.01f, false, skin);
        restitutionSlider.setWidth(150.0f);
        restitutionSlider.setValue(0.5f);
        restitutionSlider.setPosition(
            halfWidth - restitutionSlider.getWidth() - Constants.WINDOW_SPACING,
            ballDescriptor.position.y - restitutionSlider.getHeight() - 35.0f
        );

        // Create the restitution text object
        restitutionLabel = new UserText(
            Color.WHITE,
            new Vector2(0.0f, 0.0f),
            "       Restitution: "
        );
        restitutionLabel.setPosition(
            new Vector2(
                restitutionSlider.getX() - 300.0f - Constants.ITEM_SPACING,
                restitutionSlider.getY() + restitutionSlider.getHeight()
            )
        );
        restitutionLabel.setObserverString("0.50");

        // Create the friction slider
        frictionSlider = new Slider(0.0f, 25.0f, 0.1f, false, skin);
        frictionSlider.setWidth(150.0f);
        frictionSlider.setValue(1.0f);
        frictionSlider.setPosition(
            halfWidth - frictionSlider.getWidth() - Constants.WINDOW_SPACING,
            restitutionSlider.getY() - frictionSlider.getHeight() - Constants.WINDOW_SPACING
        );

        // Create the friction text object
        frictionLabel = new UserText(
            Color.WHITE,
            new Vector2(0.0f, 0.0f),
            "          Friction: "
        );
        frictionLabel.setPosition(
            new Vector2(
                restitutionSlider.getX() - 300.0f - Constants.ITEM_SPACING,
                frictionSlider.getY() + frictionSlider.getHeight()
            )
        );
        frictionLabel.setObserverString("1.00");

        // Create the density slider
        densitySlider = new Slider(0.0f, 50.0f, 0.1f, false, skin);
        densitySlider.setWidth(150.0f);
        densitySlider.setValue(5.0f);
        densitySlider.setPosition(
            halfWidth - densitySlider.getWidth() - Constants.WINDOW_SPACING,
            frictionSlider.getY() - densitySlider.getHeight() - Constants.WINDOW_SPACING
        );

        // Create the density text object
        densityLabel = new UserText(
            Color.WHITE,
            new Vector2(0.0f, 0.0f),
            "           Density: "
        );
        densityLabel.setPosition(
            new Vector2(
                densitySlider.getX() - 300.0f - Constants.ITEM_SPACING,
                densitySlider.getY() + densitySlider.getHeight()
            )
        );
        densityLabel.setObserverString("5.00");

        // Create the cannon descriptor text object
        cannonDescriptor = new UserText(
            Constants.BRIGHT_ORANGE,
            new Vector2(0.0f, 0.0f),
            "    Cannon Properties"
        );
        cannonDescriptor.setPosition(
            new Vector2(
                halfWidth - Constants.DESCRIPTOR_SPACING - Constants.WINDOW_SPACING,
                densitySlider.getY() - Constants.WINDOW_SPACING
            )
        );

        // Create the velocity `x` slider
        vxSlider = new Slider(0.0f, 25.0f, 0.1f, false, skin);
        vxSlider.setWidth(150.0f);
        vxSlider.setValue(0.0f);
        vxSlider.setPosition(
            halfWidth - vxSlider.getWidth() - Constants.WINDOW_SPACING,
            cannonDescriptor.position.y - vxSlider.getHeight() - 35.0f
        );

        // Create the velocity `x` text object
        vxLabel = new UserText(
            Color.WHITE,
            new Vector2(0.0f, 0.0f),
            "       Velocity(x): "
        );
        vxLabel.setPosition(
            new Vector2(
                vxSlider.getX() - 300.0f - Constants.ITEM_SPACING,
                vxSlider.getY() + vxSlider.getHeight()
            )
        );
        vxLabel.setObserverString("0.00");

        // Create the velocity `y` slider
        vySlider = new Slider(0.0f, 25.0f, 0.1f, false, skin);
        vySlider.setWidth(150.0f);
        vySlider.setValue(0.0f);
        vySlider.setPosition(
            halfWidth - vySlider.getWidth() - Constants.WINDOW_SPACING,
            vxSlider.getY() - vySlider.getHeight() - Constants.WINDOW_SPACING
        );

        // Create the velocity `y` text object
        vyLabel = new UserText(
            Color.WHITE,
            new Vector2(0.0f, 0.0f),
            "       Velocity(y): "
        );
        vyLabel.setPosition(
            new Vector2(
                vxSlider.getX() - 300.0f - Constants.ITEM_SPACING,
                vySlider.getY() + vySlider.getHeight()
            )
        );
        vyLabel.setObserverString("0.00");

        // Create the torque slider
        torqueSlider = new Slider(-50.0f, 50.0f, 0.1f, false, skin);
        torqueSlider.setWidth(150.0f);
        torqueSlider.setValue(0.0f);
        torqueSlider.setPosition(
            halfWidth - torqueSlider.getWidth() - Constants.WINDOW_SPACING,
            vySlider.getY() - torqueSlider.getHeight() - Constants.WINDOW_SPACING
        );

        // Create the torque text object
        torqueLabel = new UserText(
            Color.WHITE,
            new Vector2(0.0f, 0.0f),
            "            Torque: "
        );
        torqueLabel.setPosition(
            new Vector2(
                torqueSlider.getX() - 300.0f - Constants.ITEM_SPACING,
                torqueSlider.getY() + torqueSlider.getHeight()
            )
        );
        torqueLabel.setObserverString("0.00");

        // Create the world descriptor text object
        worldDescriptor = new UserText(
            Constants.BRIGHT_ORANGE,
            new Vector2(0.0f, 0.0f),
            "     World Properties"
        );
        worldDescriptor.setPosition(
            new Vector2(
                halfWidth - Constants.DESCRIPTOR_SPACING - Constants.WINDOW_SPACING,
                torqueSlider.getY() - Constants.WINDOW_SPACING
            )
        );

        // Create the gravity `x` slider
        gxSlider = new Slider(-25.0f, 25.0f, 0.1f, false, skin);
        gxSlider.setWidth(150.0f);
        gxSlider.setValue(0.00f);
        gxSlider.setPosition(
            halfWidth - gxSlider.getWidth() - Constants.WINDOW_SPACING,
            worldDescriptor.position.y - gxSlider.getHeight() - 35.0f
        );

        // Create the gravity `x` text object
        gxLabel = new UserText(
            Color.WHITE,
            new Vector2(0.0f, 0.0f),
            "        Gravity(x): "
        );
        gxLabel.setPosition(
            new Vector2(
                gxSlider.getX() - 300.0f - Constants.ITEM_SPACING,
                gxSlider.getY() + gxSlider.getHeight()
            )
        );
        gxLabel.setObserverString("0.00");

        // Create the gravity `y` slider
        gySlider = new Slider(-25.0f, 25.0f, 0.1f, false, skin);
        gySlider.setWidth(150.0f);
        gySlider.setValue(-9.80f);
        gySlider.setPosition(
            halfWidth - gySlider.getWidth() - Constants.WINDOW_SPACING,
            gxSlider.getY() - gySlider.getHeight() - Constants.WINDOW_SPACING
        );

        // Create the gravity `y` text object
        gyLabel = new UserText(
            Color.WHITE,
            new Vector2(0.0f, 0.0f),
            "        Gravity(y): "
        );
        gyLabel.setPosition(
            new Vector2(
                gxSlider.getX() - 300.0f - Constants.ITEM_SPACING,
                gySlider.getY() + gySlider.getHeight()
            )
        );
        gyLabel.setObserverString("-9.80");

        // Create a resume button
        resumeButton = new TextButton("Resume", skin, "default");
        resumeButton.setWidth(150.0f);
        resumeButton.setHeight(35.0f);
        resumeButton.setPosition(
            halfWidth - resumeButton.getWidth() - Constants.WINDOW_SPACING,
            gySlider.getY() - resumeButton.getHeight() - Constants.ITEM_SPACING - 10.0f
        );

        // Create an exit button
        exitButton = new TextButton("Exit", skin, "default");
        exitButton.setWidth(150.0f);
        exitButton.setHeight(35.0f);
        exitButton.setPosition(
            halfWidth - exitButton.getWidth() - Constants.WINDOW_SPACING,
            resumeButton.getY() - exitButton.getHeight() - Constants.ITEM_SPACING - 5.0f
        );
    }

    /**
     * Adds pause menu actors to the given stage (order matters).
     * @param stage
     */
    public void addActors(Stage stage) {
        // Add the background
        stage.addActor(background);

        // Add menu descriptors
        stage.addActor(ballDescriptor);
        stage.addActor(cannonDescriptor);
        stage.addActor(worldDescriptor);

        // Add restitution slider and label
        stage.addActor(restitutionSlider);
        stage.addActor(restitutionLabel);

        // Add friction slider and label
        stage.addActor(frictionSlider);
        stage.addActor(frictionLabel);

        // Add density slider and label
        stage.addActor(densitySlider);
        stage.addActor(densityLabel);

        // Add velocity `x` and `y` slider and label
        stage.addActor(vxSlider);
        stage.addActor(vxLabel);
        stage.addActor(vySlider);
        stage.addActor(vyLabel);

        // Add torque slider and label
        stage.addActor(torqueSlider);
        stage.addActor(torqueLabel);

        // Add gravity `x` and `y` slider and label
        stage.addActor(gxSlider);
        stage.addActor(gxLabel);
        stage.addActor(gySlider);
        stage.addActor(gyLabel);

        // Add resume and exit buttons
        stage.addActor(resumeButton);
        stage.addActor(exitButton);
    }

    /**
     * Removes pause menu actors from the most recent stage (order does not matter).
     */
    public void removeActors() {
        // Remove all actors from the stage
        background.remove();

        // Remove menu descriptors
        ballDescriptor.remove();
        cannonDescriptor.remove();
        worldDescriptor.remove();

        // Remove restitution slider and label
        restitutionSlider.remove();
        restitutionLabel.remove();

        // Remove friction slider and label
        frictionSlider.remove();
        frictionLabel.remove();

        // Remove density slider and label
        densitySlider.remove();
        densityLabel.remove();

        // Remove velocity `x` and `y` slider and label
        vxSlider.remove();
        vxLabel.remove();
        vySlider.remove();
        vyLabel.remove();

        // Remove torque slider and label
        torqueSlider.remove();
        torqueLabel.remove();

        // Remove gravity `x` and `y` slider and label
        gxSlider.remove();
        gxLabel.remove();
        gySlider.remove();
        gyLabel.remove();

        // Remove resume and exit buttons
        resumeButton.remove();
        exitButton.remove();
    }

    /**
     * Deallocates all disposable data associated with the main menu.
     */
    public void dispose() {
        skin.dispose();
        background.dispose();
        ballDescriptor.dispose();
        cannonDescriptor.dispose();
        worldDescriptor.dispose();
        restitutionLabel.dispose();
        frictionLabel.dispose();
        densityLabel.dispose();
        vxLabel.dispose();
        vyLabel.dispose();
        torqueLabel.dispose();
        gxLabel.dispose();
        gyLabel.dispose();
        background.dispose();
    }
}
