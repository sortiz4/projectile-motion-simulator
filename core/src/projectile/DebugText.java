package projectile;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

/**
 * A predefined text object with a modifiable color space (for debugging).
 */
public class DebugText extends TextObject {
    /**
     * Creates an instance of this text object with basic information.
     * @param color
     * @param position
     * @param observer
     */
    public DebugText(Color color, Vector2 position, Observer observer) {
        super("ui/font/consolas-12.fnt", "ui/font/consolas-12.png");
        super.setColor(color);
        super.setObserver(observer);
        super.setPosition(position);
    }
}
