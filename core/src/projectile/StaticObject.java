package projectile;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;

/**
 * A static object assumes that a body will always be created (a texture is not necessary).
 */
public class StaticObject extends GameObject {
    /**
     * Creates an untextured static object (can only be an edge fixture).
     * @param world
     * @param left
     * @param right
     */
    public StaticObject(World world, Vector2 left, Vector2 right) {
        super(BodyDef.BodyType.StaticBody);
        super.createEdgeFixture(left, right);
        super.initializePosition(world, new Vector2(0, 0));
    }

    /**
     * Creates a textured static object (can only be a circle or polygon).
     * @param world
     * @param shapeType
     * @param texturePath
     * @param position
     */
    public StaticObject(World world, String shapeType, String texturePath, Vector2 position) {
        super(BodyDef.BodyType.StaticBody);
        super.createSprite(texturePath);
        super.initializePosition(world, position);

        if (shapeType.equals("CircleShape")) {
            super.createCircleFixture(null);
        } else if (shapeType.equals("PolygonShape")) {
            super.createPolygonFixture(null);
        }
    }
}
